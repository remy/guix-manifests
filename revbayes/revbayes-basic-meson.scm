(define-module (revbayes)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (guix git-download)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages boost))

(define-public revbayes
  (package
    (name "revbayes")
    (version "1.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/revbayes/revbayes/")
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "04074876b519vs30lw14ml8d4wwagba8lxn9zaf3p0q4z1gwpcxj"))))
    (build-system meson-build-system)
    (arguments 
      '(#:tests? #f ; TODO tests pass except FBD and UCLD_noncentered, to desactivate
        #:build-type "release"
        #:phases
            (modify-phases %standard-phases
              (add-before 'configure 'generate-meson
                (lambda _
                  (invoke "bash" "projects/meson/generate.sh"))))))
    (native-inputs
     (list pkg-config boost perl))
    (home-page "https://revbayes.github.io/")
    (synopsis "RevBayes")
    (description "RevBayes provides an interactive environment for statistical computation in phylogenetics. It is primarily intended for modeling, simulation, and Bayesian inference in evolutionary biology, particularly phylogenetics. However, the environment is quite general and can be useful for many complex modeling tasks.

RevBayes uses its own language, Rev, which is a probabilistic programming language like JAGS, STAN, Edward, PyMC3, and related software. However, phylogenetic models require inference machinery and distributions that are unavailable in these other tools.
    
The Rev language is similar to the language used in R. Like the R language, Rev is designed to support interactive analysis. It supports both functional and procedural programming models, and makes a clear distinction between the two. Rev is also more strongly typed than R.
    
RevBayes is a collaboratively developed software project.")
    (license license:gpl3+)))

revbayes
